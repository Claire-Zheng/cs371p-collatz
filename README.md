# CS371p: Object-Oriented Programming Collatz Repo

* Name: Claire Zheng

* EID: cz5977

* GitLab ID: Claire-Zheng

* HackerRank ID: Clayuh

* Git SHA: c46170688b7d39c354e16623d466032419cc18de

* GitLab Pipelines: https://gitlab.com/Claire-Zheng/cs371p-collatz/-/pipelines

* Estimated completion time: 8

* Actual completion time: 6

* Comments: N/A
