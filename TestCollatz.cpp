// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read0) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

TEST(CollatzFixture, read1) {
    ASSERT_EQ(collatz_read("3 3\n"), make_pair(3, 3));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(10, 20)), make_tuple(10, 20, 21));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(20, 30)), make_tuple(20, 30, 112));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(27, 27)), make_tuple(27, 27, 112));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(20, 30)), make_tuple(20, 30, 112));
}

// -----
// print
// -----

TEST(CollatzFixture, print0) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

TEST(CollatzFixture, print1) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 1, 1));
    ASSERT_EQ(sout.str(), "1 1 1\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve0) {
    istringstream sin("");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "");
}

TEST(CollatzFixture, solve1) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

TEST(CollatzFixture, solve2) {
    istringstream sin("1 10\n100 200\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n");
}

TEST(CollatzFixture, solve3) {
    istringstream sin("1 1\n2 2\n3 3\n4 4\n5 5\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 1 1\n2 2 2\n3 3 8\n4 4 3\n5 5 6\n");
}

TEST(CollatzFixture, solve4) {
    istringstream sin("1 1\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 1 1\n");
}

TEST(CollatzFixture, solve5) {
    istringstream sin("1 100\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 100 119\n");
}

TEST(CollatzFixture, solve6) {
    istringstream sin("1 100\n 30 100\n 60 100\n 90 100\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 100 119\n30 100 119\n60 100 119\n90 100 119\n");
}

TEST(CollatzFixture, solve7) {
    istringstream sin("1 999999\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 999999 525\n");
}