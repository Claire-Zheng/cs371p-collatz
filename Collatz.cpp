// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

const int CACHE_SIZE = 10000000;
int others[CACHE_SIZE];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;

    // make sure i and j are both within range
    assert(i > 0);
    assert(i < 1000000);
    assert(j > 0);
    assert(j < 1000000);

    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;

    // make sure begin < end to handle reversed inputs
    int begin = i;
    int end = j;
    if (end < begin) std::swap(end, begin);

    // optimization to only have to calculate back half
    int half = (end/2) + 1;
    begin = (begin < half) ? half : begin;

    // initialize our cache for the base case n = 1
    others[1] = 1;

    int max_cycles = 0;
    int cycles;
    for (long curr = begin; curr <= end; curr++) {
        cycles = (int) collatz_cycle(curr);
        if (cycles > max_cycles) max_cycles = cycles;
    }

    assert(max_cycles > 0);
    return make_tuple(i, j, max_cycles);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    int counter = 0;
    while (getline(sin, s)) {
        collatz_print(sout, collatz_eval(collatz_read(s)));

        // makes sure # of pairs evaluated stays below 1000
        assert(counter++ < 1000);
    }
}

// -------------
// collatz_cycle
// -------------

int collatz_cycle(long n) {
    // if n is in the cache, return it
    if (n < CACHE_SIZE && others[n] != 0) return others[n];

    // if n can be cached, cache it
    if (n < CACHE_SIZE) {
        if (n % 2 == 0) others[n] = 1 + collatz_cycle(n/2);
        else others[n] = 2 + collatz_cycle((3*n + 1)/2);
        // otherwise, we'll have to manually calculate it and hit cache value later on
    } else {
        if (n % 2 == 0) return 1 + collatz_cycle(n/2);
        else return 2 + collatz_cycle((3*n + 1)/2);
    }

    return others[n];
}